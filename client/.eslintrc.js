const fs = require("fs");
const typeDefs = fs.readFileSync('../_shared/schema.gql').toString();

module.exports = {
  parser: "babel-eslint",
  extends: "react-app",
  rules: {
    "graphql/template-strings": [
      "error",
      {
        // Import default settings for your GraphQL client. Supported values:
        // 'apollo', 'relay', 'lokka', 'literal'
        env: "relay",

        // Import your schema JSON here
        schemaString: typeDefs

        // OR provide absolute path to your schema JSON
        // schemaJsonFilepath: path.resolve(__dirname, './schema.json'),

        // OR provide the schema in the Schema Language format
        // schemaString: printSchema(schema),

        // tagName is set for you to Relay.QL
      }
    ]
  },
  plugins: ["graphql"]
};
