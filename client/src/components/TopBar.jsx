import React from "react";
import { Navbar } from "react-bootstrap";
import { QueryRenderer, graphql } from "react-relay";
import environment from "../Enviroment";

const TopBar = props => {
  return (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>Veiculos</Navbar.Brand>
    </Navbar.Header>
    <Navbar.Collapse>
      <Navbar.Text pullRight>
        Veiculos Inativos: {props.veiculosInativos}
      </Navbar.Text>
    </Navbar.Collapse>
  </Navbar>
)};

const TopBarQueryRenderer = () => {
  return (
    <QueryRenderer
      environment={environment}
      query={graphql`
        query TopBarQuery {
          veiculos {
            id
            inativosCount
          }
        }
      `}
      render={({ error, props }) => {
        if (error) {
          return <div>{error.message}</div>;
        } else if (props) {
          console.log(props);
          return <TopBar veiculosInativos={props.veiculos.inativosCount} />;
        }
        return <div>Loading</div>;
      }}
    />
  );
};

export default TopBarQueryRenderer;
