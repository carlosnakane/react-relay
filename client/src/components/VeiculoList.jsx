// @flow

import React from "react";
import VeiculoListItem from "./VeiculoListItem";
import { createFragmentContainer, graphql } from "react-relay";
import { ListGroup, ListGroupItem } from "react-bootstrap";
import type { VeiculoList_veiculos } from "./__generated__/VeiculoList_veiculos.graphql";

type Props = {
  veiculos: VeiculoList_veiculos,
  onSelect(id: string): void
};

const VeiculosList = (props: Props) => {
  return (
    <div>
      <ListGroup>
        {props.veiculos.lista.map(veiculo => (
          <ListGroupItem
            key={veiculo.id}
            onClick={() => props.onSelect(veiculo.id)}
          >
            <VeiculoListItem veiculo={veiculo} />
          </ListGroupItem>
        ))}
      </ListGroup>
    </div>
  );
};

export default createFragmentContainer(VeiculosList, {
  veiculos: graphql`
    fragment VeiculoList_veiculos on Veiculos {
      lista {
        ...VeiculoListItem_veiculo
        id
        inativo
      }
    }
  `
});
