// @flow

import React from "react";
import { createFragmentContainer, graphql } from "react-relay";
import { Label } from "react-bootstrap";
import type { VeiculoListItem_veiculo } from "./__generated__/VeiculoListItem_veiculo.graphql";

type Props = {
  veiculo: VeiculoListItem_veiculo
};

const VeiculoListItem = (props: Props) => {
  return (
    <span>
      <Label bsStyle={props.veiculo.inativo ? "danger" : "success"}>
        {props.veiculo.placa}
      </Label>
      <span>{props.veiculo.cor}</span>
    </span>
  );
};

export default createFragmentContainer(VeiculoListItem, {
  veiculo: graphql`
    fragment VeiculoListItem_veiculo on Veiculo {
      placa
      cor
      inativo
      tara
    }
  `
});
