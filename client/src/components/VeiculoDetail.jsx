// @flow

import React, { Component } from "react";
import { Panel, FormGroup, Radio } from "react-bootstrap";
import { createFragmentContainer, graphql } from "react-relay";
import ChangeVeiculoInativoMutation from "../mutations/ChangeVeiculoInativoMutation";
import type { VeiculoDetail_veiculo } from "./__generated__/VeiculoDetail_veiculo.graphql";
import { ConcreteRequest } from "relay-runtime";

type Props = {
  veiculo: VeiculoDetail_veiculo
};

class VeiculoDetail extends Component<Props, *> {
  onInativoChange = inativo => {
    ChangeVeiculoInativoMutation(this.props.veiculo.id, inativo, () => {
      console.log("Mutation complete");
    });
  };

  render() {
    console.log(ConcreteRequest);
    return (
      <Panel>
        <Panel.Heading>{this.props.veiculo.placa}</Panel.Heading>
        <Panel.Body collapsible={false}>
          <ul>
            <li>Cor: {this.props.veiculo.cor}</li>
            <li>Tara: {this.props.veiculo.tara}</li>
            <li>
              Inativo:
              <FormGroup>
                <Radio
                  name="radioGroup"
                  inline
                  readOnly
                  checked={this.props.veiculo.inativo}
                  onClick={() => {
                    this.onInativoChange(true);
                  }}
                >
                  SIM
                </Radio>
                <Radio
                  name="radioGroup"
                  inline
                  readOnly
                  checked={!this.props.veiculo.inativo}
                  onClick={() => {
                    this.onInativoChange(false);
                  }}
                >
                  NÃO
                </Radio>
              </FormGroup>
            </li>
          </ul>
        </Panel.Body>
      </Panel>
    );
  }
}

export default createFragmentContainer(VeiculoDetail, {
  veiculo: graphql`
    fragment VeiculoDetail_veiculo on Veiculo {
      id
      placa
      cor
      tara
      inativo
    }
  `
});
