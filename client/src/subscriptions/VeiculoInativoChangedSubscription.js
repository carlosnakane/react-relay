import { graphql, requestSubscription } from "react-relay";
import environment from "../Enviroment";

const veiculoInativoChangedSubscription = graphql`
  subscription VeiculoInativoChangedSubscription {
    veiculoInativoChanged {
      veiculo {
        id
        placa
        inativo
      }
      totalInativo
    }
  }
`;

export default () => {
  const subscriptionConfig = {
    subscription: veiculoInativoChangedSubscription,
    updater: proxyStore => {
      const root = proxyStore.getRootField('veiculoInativoChanged');
      const updatedVeiculo = root.getLinkedRecord('veiculo');

      const updatedVeiculoId = updatedVeiculo.getValue('id');
      const updatedVeiculoInativoValue = updatedVeiculo.getValue('inativo');

      const currentVeiculoState = proxyStore.get(updatedVeiculoId);
      currentVeiculoState.setValue(updatedVeiculoInativoValue, 'inativo');
    }
  };

  requestSubscription(environment, subscriptionConfig);
};
