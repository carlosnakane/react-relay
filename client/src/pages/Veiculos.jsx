// @flow

import React, { Component } from "react";
import VeiculoList from "../components/VeiculoList";
import VeiculoDetail from "../components/VeiculoDetail";
import { Grid, Row, Col } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import { QueryRenderer, graphql } from "react-relay";
import environment from "../Enviroment";
import { Route } from "react-router-dom";
import VeiculoInativoChangedSubscription from "../subscriptions/VeiculoInativoChangedSubscription";
import type { VeiculosQueryResponse } from './__generated__/VeiculosQuery.graphql';

type Props = {} & VeiculosQueryResponse;

class Veiculos extends Component<Props, *> {
  onSelect = veiculoId => {
    this.props.history.push(`/veiculos/${veiculoId}`);
  };

  render() {
    return (
      <Grid>
        <Row>
          <Col md={5}>
            <VeiculoList
              veiculos={this.props.veiculos}
              onSelect={this.onSelect}
            />
          </Col>
          <Col md={7}>
            <Route
              path={`/veiculos/:veiculoId`}
              render={() => <VeiculoDetail veiculo={this.props.veiculo} />}
            />
          </Col>
        </Row>
      </Grid>
    );
  }

  componentDidMount() {
    new VeiculoInativoChangedSubscription();
  }
}

const VeiculosWithRouter = withRouter(Veiculos);

const VeiculosQueryRenderer = withRouter(props => {
  const {
    match: { params }
  } = props;
  return (
    <QueryRenderer
      environment={environment}
      query={graphql`
        query VeiculosQuery($veiculoId: String!, $includeVeiculo: Boolean!) {
          veiculos {
            ...VeiculoList_veiculos
            id
          }
          veiculo(id: $veiculoId) @include(if: $includeVeiculo) {
            ...VeiculoDetail_veiculo
          }
        }
      `}
      variables={{
        veiculoId: params.veiculoId || "",
        includeVeiculo: params.veiculoId != null
      }}
      render={({ error, props }) => {
        if (error) {
          return <div>{error.message}</div>;
        } else if (props) {
          return (
            <VeiculosWithRouter
              veiculos={props.veiculos}
              veiculo={props.veiculo}
            />
          );
        }
        return <div>Loading</div>;
      }}
    />
  );
});

export default VeiculosQueryRenderer;
