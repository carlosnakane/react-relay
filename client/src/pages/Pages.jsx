// @flow

import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Veiculos from "./Veiculos";
import TopBar from '../components/TopBar';

const Pages = () => (
  <div>
    <TopBar />
    <div style={{ marginTop: 30 }}>
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/veiculos" />} />
        <Route exact path="/veiculos/:veiculoId?" component={Veiculos} />
      </Switch>
    </div>
  </div>
);

export default Pages;
