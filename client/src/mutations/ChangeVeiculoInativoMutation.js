import { commitMutation, graphql } from 'react-relay';
import enviroment from '../Enviroment';

const mutation = graphql`
  mutation ChangeVeiculoInativoMutation($veiculoId: ID!, $inativo: Boolean!) {
    changeVeiculoInativo(veiculoId: $veiculoId, inativo: $inativo) {
      inativosCount
      lista {
        inativo
      }
    }
  }
`

export default (veiculoId, inativo, callback) => {
  const variables = {
    veiculoId,
    inativo,
  }

  commitMutation(
    enviroment,
    {
      mutation,
      variables,
      onCompleted: (a) => {
        callback(a)
      },
      onError: (error) => {
        console.log(error);
      }
    }
  )
}
