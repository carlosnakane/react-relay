import React, { Component } from "react";
import "./App.css";
import Pages from "./pages/Pages";
import { BrowserRouter } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Pages />
      </BrowserRouter>
    );
  }
}

export default App;
