import { Environment, Network, RecordSource, Store } from "relay-runtime";
import { SubscriptionClient } from "subscriptions-transport-ws";

const store = new Store(new RecordSource());
const serverIp = '172.30.8.130';

const fetchQuery = (operation, variables) => {
  return fetch(`http://${serverIp}:4000/graphql`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      query: operation.text,
      variables
    })
  }).then(response => {
    return response.json();
  });
};

const setupSubscription  = (config, variables, cacheConfig, observer) => {
  const query = config.text;
  const subscriptionClient = new SubscriptionClient(
    `ws://${serverIp}:4000/subscriptions`,
    { reconnect: true }
  );
  subscriptionClient.subscribe({ query, variables }, (error, result) => {
    observer.onNext({ data: result });
  });
};

const network = Network.create(fetchQuery, setupSubscription );

const enviroment = new Environment({ store, network });

export default enviroment;
