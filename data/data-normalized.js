const centroCusto = [
  {
    id: "27f08284-7f26-4fe5-a148-2cc692e8e3f6",
    code: 5,
    nome: "101 - PUXADA",
    inativo: false,
    updateToken: "ccd35f81-bc1e-48c4-80c5-14548179d859"
  },
  {
    id: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    code: 2,
    nome: "ENTREGA",
    inativo: false,
    updateToken: "8768f671-ec54-433a-963c-a95108147b58"
  },
  {
    id: "d77f2d59-41e9-485b-8c9b-60d68d3fa41b",
    code: 1,
    nome: "CC ENTREGA - 501",
    inativo: false,
    updateToken: "00000000-0000-0000-0000-000000000000"
  }
];

const chassi = [
  {
    id: "d78e92ad-e23a-45a0-840d-10073b855cc7",
    code: 2,
    nome: "Traçado",
    eixos: [
      { tipo: 1, slots: 4, posicao: 2 },
      { tipo: 1, slots: 4, posicao: 3 },
      { tipo: 0, slots: 2, posicao: 1 }
    ],
    inativo: false,
    updateToken: "07295588-9018-4c5f-82b7-a44a4762076e",
    engateTraseiro: 0,
    engateDianteiro: 0
  },
  {
    id: "ade64faa-5b4f-47f6-9f5d-3be6a51235b3",
    code: 1,
    nome: "Toco",
    eixos: [
      { tipo: 0, slots: 2, posicao: 1 },
      { tipo: 1, slots: 4, posicao: 2 }
    ],
    inativo: false,
    updateToken: "d6bda682-ba79-44cb-ba9e-8fc6943027c2",
    engateTraseiro: 0,
    engateDianteiro: 0
  },
  {
    id: "b10e8331-75e8-4958-a17b-14d9d9a9ddf3",
    code: 6,
    nome: "Cavalo Mecânico Trucado",
    eixos: [
      { tipo: 1, slots: 4, posicao: 2 },
      { tipo: 0, slots: 2, posicao: 1 },
      { tipo: 1, slots: 4, posicao: 3 }
    ],
    inativo: false,
    updateToken: "eb616a10-67b1-4cc4-8109-5466868c0a3d",
    engateTraseiro: 1,
    engateDianteiro: 0
  },
  {
    id: "9ce062f0-5981-401e-902a-4c8bc51b37f7",
    code: 17,
    nome: "Semirreboque Triplo Quatro Rodas",
    eixos: [
      { tipo: 2, slots: 4, posicao: 2 },
      { tipo: 2, slots: 4, posicao: 1 },
      { tipo: 2, slots: 4, posicao: 3 }
    ],
    inativo: false,
    updateToken: "fd4eda06-2c52-4271-8574-a5db5ad1b048",
    engateTraseiro: 0,
    engateDianteiro: 1
  },
  {
    id: "f2e0bbe2-325f-4902-81cc-7c2a1778a2d1",
    code: 12,
    nome: "Utilitário",
    eixos: [
      { tipo: 3, slots: 2, posicao: 1 },
      { tipo: 2, slots: 2, posicao: 2 }
    ],
    inativo: false,
    updateToken: "efadf28e-7767-40dc-aba0-fca79ac005b8",
    engateTraseiro: 0,
    engateDianteiro: 0
  }
];

const veiculoModelo = [
  {
    id: "a90ee09d-4097-2d2e-2690-e7ea401c112a",
    nome: "R440 Griffin Edition ",
    chassiId: "d78e92ad-e23a-45a0-840d-10073b855cc7"
  },
  {
    id: "21cc12bf-8ff8-65aa-adb4-6436dda1daa3",
    nome: "CARGO 1722 E",
    chassiId: "ade64faa-5b4f-47f6-9f5d-3be6a51235b3"
  },
  {
    id: "c305aba9-37ba-9f29-1bcb-fe882d235d9f",
    nome: "9150 WORkER/ FECHADA",
    chassiId: "ade64faa-5b4f-47f6-9f5d-3be6a51235b3"
  },
  {
    id: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    nome: "ATEGO 1719",
    chassiId: "d78e92ad-e23a-45a0-840d-10073b855cc7"
  },
  {
    id: "f33f620f-be44-43f9-8588-c6b4c59e1cdf",
    nome: "TESTE F",
    chassiId: "b10e8331-75e8-4958-a17b-14d9d9a9ddf3"
  },
  {
    id: "8f22d70f-ce0a-49bd-b5d8-e658acbe00b8",
    nome: "TESTE",
    chassiId: "9ce062f0-5981-401e-902a-4c8bc51b37f7"
  },
  {
    id: "2ceb8bd6-eec5-4ae3-af66-705cdcc6f69d",
    nome: "WV 17.180 WORKER",
    chassiId: "f2e0bbe2-325f-4902-81cc-7c2a1778a2d1"
  }
];

const veiculo = [
  {
    id: "a1a46d51-69df-47bb-3461-6d66dc57cbfc",
    cor: "Branco",
    antt: 1234,
    tara: 2500,
    placa: "RRR7777",
    centroCustoId: "27f08284-7f26-4fe5-a148-2cc692e8e3f6",
    veiculoModelId: "a90ee09d-4097-2d2e-2690-e7ea401c112a",
    inativo: false,
  },
  {
    id: "540663f8-0398-3ef2-d226-ef5d4b7e01c3",
    cor: "VERMELHA / BRAHMA",
    antt: 999,
    tara: 99,
    placa: "IOA1043",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "21cc12bf-8ff8-65aa-adb4-6436dda1daa3",
    inativo: true,
  },
  {
    id: "bcea166d-8d15-f585-b8dc-ce00086e0d76",
    cor: "Branco",
    antt: 9900,
    tara: 2800,
    placa: "NYP1196",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "c305aba9-37ba-9f29-1bcb-fe882d235d9f",
    inativo: false,
  },
  {
    id: "523e2a41-35ff-9e0a-0205-e1381978571e",
    cor: "AZUL/ANTARCTICA",
    antt: 99999,
    tara: 99,
    placa: "BBB2409",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  },
  {
    id: "1ede3507-fb2a-499c-8cb4-0cf75debd9c7",
    cor: "PRETO",
    tara: 5000,
    placa: "PQT8281",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  },
  {
    id: "cc777876-53ec-49d5-95ff-3b816a6af79a",
    cor: "Rosa",
    tara: 8000,
    placa: "TST9998",
    centroCustoId: "d77f2d59-41e9-485b-8c9b-60d68d3fa41b",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  },
  {
    id: "9a534ec8-f36e-43fc-ad3c-f9c6fe5458bc",
    cor: "VERDE",
    tara: 5000,
    placa: "ALU2329",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  },
  {
    id: "df45d6be-0a67-4234-8d45-69bb2157ab0d",
    cor: "BRANCA",
    tara: 4500,
    placa: "PCB4360",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  },
  {
    id: "525f2f3a-2888-4605-9def-47686b45d0e8",
    cor: "preto",
    tara: 7500,
    placa: "TST0004",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "f33f620f-be44-43f9-8588-c6b4c59e1cdf",
    inativo: false,
  },
  {
    id: "2601c85f-57e7-41e4-b803-ec36317267b8",
    cor: "preto",
    tara: 7500,
    placa: "TST0003",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "8f22d70f-ce0a-49bd-b5d8-e658acbe00b8",
    inativo: false,
  },
  {
    id: "039a3ced-fe54-4da9-a7a5-b2709ff956f5",
    cor: "branca",
    tara: 5000,
    placa: "TST0002",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  },
  {
    id: "20395b7c-8d23-4828-a096-e2a9be81c733",
    cor: "preto",
    tara: 5020,
    placa: "FTW7690",
    centroCustoId: "d77f2d59-41e9-485b-8c9b-60d68d3fa41b",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  },
  {
    id: "637a1b70-0613-4f92-9c44-dcef36cfe4b3",
    cor: "VERMELHO",
    tara: 999,
    placa: "AAA2222",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "2ceb8bd6-eec5-4ae3-af66-705cdcc6f69d",
    inativo: false,
  },
  {
    id: "e00f6c54-8abc-4623-9ecc-5f86c469b5fd",
    cor: "preto",
    tara: 5300,
    placa: "TES4321",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  },
  {
    id: "8f527f33-02b0-4a69-b2f7-c2d7d86618b5",
    cor: "VERDE",
    antt: 12345678,
    tara: 99,
    placa: "HBS1410",
    centroCustoId: "2ca3b948-ac79-4cc4-b068-ff03f6b271d9",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  },
  {
    id: "cbe63fe0-c8c6-487d-b3d8-eceed0c4fe2d",
    cor: "A",
    tara: 99,
    placa: "AAA2209",
    centroCustoId: "d77f2d59-41e9-485b-8c9b-60d68d3fa41b",
    veiculoModelId: "2ceb8bd6-eec5-4ae3-af66-705cdcc6f69d",
    inativo: false,
  },
  {
    id: "99d5e6b7-c8a1-4d91-b29c-142654cb9f59",
    cor: "preto",
    tara: 2500,
    placa: "TST9999",
    centroCustoId: "d77f2d59-41e9-485b-8c9b-60d68d3fa41b",
    veiculoModelId: "eabad206-cd69-4e75-84d9-e5986ff28a8f",
    inativo: false,
  }
];

module.exports = { centroCusto, chassi, veiculoModelo, veiculo };
