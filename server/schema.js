const makeExecutableSchema = require('graphql-tools').makeExecutableSchema;
const fs = require("fs");
const resolvers = require('./resolvers').resolvers;

const typeDefs = fs.readFileSync('../_shared/schema.gql').toString();

const schema = makeExecutableSchema({ typeDefs, resolvers });
module.exports = { schema };