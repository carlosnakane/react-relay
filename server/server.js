const express = require("express");
const { execute, subscribe } = require("graphql");
const cors = require("cors");
const SubscriptionServer = require("subscriptions-transport-ws").SubscriptionServer;
const createServer = require("http").createServer;
const bodyParser = require('body-parser');
const graphqlExpress = require('apollo-server-express').graphqlExpress;
const graphiqlExpress = require('apollo-server-express').graphiqlExpress;

const schema = require('./schema').schema;


const PORT = 4000;

const server = express();
server.use(cors());

server.use('/graphql', bodyParser.json(), graphqlExpress({
  schema
}));

server.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
  subscriptionsEndpoint: `ws://localhost:${PORT}/subscriptions`
}));


const ws = createServer(server);

ws.listen(PORT, () => {
  console.log(`Apollo Server is now running on http://localhost:${PORT}`);
  new SubscriptionServer({
    execute,
    subscribe,
    schema
  }, {
    server: ws,
    path: '/subscriptions',
  });
});
