const PubSub = require("graphql-subscriptions").PubSub;

const veiculoStore = require("../data/data-normalized").veiculo;
const centroCustoStore = require("../data/data-normalized").centroCusto;
const veiculoModeloStore = require("../data/data-normalized").veiculoModelo;

const pubsub = new PubSub();

const VEICULO_INATIVO_CHANGED = "veiculoInativoChanged";

const getVeiculo = args => {
  const id = args.id;
  const veiculoFound = veiculoStore.find(veiculo => veiculo.id === id);
  if (veiculoStore == null) {
    console.log("Veiculo not found");
  }
  return veiculoFound;
};

const getCentroCusto = args => {
  const id = args.id;
  return centroCustoStore.find(centroCusto => centroCusto.id === id);
};

const getVeiculoModelo = args => {
  const id = args.id;
  return veiculoModeloStore.find(veiculoModelo => veiculoModelo.id === id);
};

const getVeiculos = () => {
  const inativos = veiculoStore.filter(v => v.inativo).length;
  return {
    id: "a659f14f-330f-492a-ad4d-4e41d20f3f39",
    lista: veiculoStore,
    inativosCount: inativos,
  }
}


const changeVeiculoInativo = ({ veiculoId, inativo }) => {
  const veiculoFound = getVeiculo({ id: veiculoId });
  veiculoFound.inativo = inativo;
  const veiculos = getVeiculos();
  pubsub.publish(VEICULO_INATIVO_CHANGED, {
    veiculoInativoChanged: { veiculo: veiculoFound, totalInativo: veiculos.inativosCount }
  });
  return veiculos;
};

const resolvers = {
  Query: {
    veiculos: getVeiculos,
    veiculo: (root, args) => {
      return getVeiculo(args);
    },
    centroCusto: (root, args) => {
      return getCentroCusto(args);
    },
    veiculoModelo: (root, args) => {
      return getVeiculoModelo(args);
    },
  },
  Mutation: {
    changeVeiculoInativo: (root, args) => {
      return changeVeiculoInativo(args);
    }
  },
  Subscription: {
    veiculoInativoChanged: {
      subscribe: () => {
        return pubsub.asyncIterator(VEICULO_INATIVO_CHANGED);
      }
    }
  }
};

module.exports = {
  resolvers
};
